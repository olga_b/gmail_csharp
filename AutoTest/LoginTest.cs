﻿using System;
using System.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Drawing;

using AutoTest.pages;

namespace AutoTest
{
    [TestFixture]
    public class LoginTest
    {
        public static string igWorkDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); // рабочий каталог, относительно исполняемого файла (в нашем случае относительно DLL)
        public static IWebDriver driver;
        public static ChromeOptions options;

        [OneTimeSetUp] 
        public void OneTimeSetUp()
        {
            options = new ChromeOptions();
            options.AddArguments("--ignore-certificate-errors");
            options.AddArguments("--ignore-ssl-errors");

        }

        [SetUp] 
        public void SetUp()
        {

            driver = new ChromeDriver(igWorkDir, options);
            driver.Manage().Window.Maximize();
        }

        [TearDown] 
        public void TearDown()
        {
            var myUniqueFileName = string.Format(@"{0}.png", DateTime.Now.Ticks);
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(myUniqueFileName);
            
            driver.Close();
            driver.Quit();
        }


        [TestCase("testolya18@gmail.com","1979492test")]
        [TestCase("testolya19@gmail.com","1979492test")]
        [TestCase("testolya18@gmail.com","1979492tes")]
        [TestCase("testolya18gmail.com","1979492test")]
        [TestCase(" testolya18@gmail.com ","1979492test")]
        [TestCase("testolya18@gmail.com"," 1979492test ")]
        [TestCase("","1979492test")]
        [TestCase("  ","1979492test")]
        [TestCase("testolya18@gmail.com","")]
        [TestCase("testolya18@gmail.com","  ")]
        
        [Test]
        public void EnterEmailTest(String login, String password)
        {
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["BaseUrl"]);
            LoginPage loginPage = new LoginPage(driver);
            WelcomePage welcomePage = loginPage.EnterEmail(login);
            InboxPage inboxPage = welcomePage.EnterPassword(password);
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
            Assert.True(inboxPage.AccountIsCorrect(login));
            inboxPage.Logout();
            
            Thread.Sleep(5000);
        }
        
    }
}
