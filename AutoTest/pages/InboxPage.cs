﻿using System;
using System.Drawing.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.pages
{
    public class InboxPage : AbstractPage
    {
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'SignOutOptions')]")]
        private IWebElement AccountBtn;

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'Logout')]")]
        private IWebElement LogoutBtn;
        
        
        public InboxPage(IWebDriver driver) : base(driver)
        {
        }

        public bool AccountIsCorrect(String mail)
        {
            
            String xpath = "//a[contains(@title,'"+mail+"')]";
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.XPath(xpath)));
            
            IWebElement title = driver.FindElement(By.XPath(xpath));
            
            return driver.FindElement(By.XPath(xpath)).Displayed;
           
        }

        public void Logout()
        {
            WaitUntil(AccountBtn);
            AccountBtn.Click();
            
            WaitUntil(LogoutBtn);
            LogoutBtn.Click();
        }
    }
}