﻿using System;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using AutoTest.GmailApi;
using NUnit.Framework;
using OpenQA.Selenium.Remote;

namespace AutoTest
{
    public class GmailTest
    {
        public static String UserId = ConfigurationManager.AppSettings["UserId"];
        public static String Query = ConfigurationManager.AppSettings["Query"];
        /// <summary>
        /// List all Messages of the user's mailbox matching the query.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="query">String used to filter Messages returned.</param>
        public static List<Message> ListMessages(GmailService service, String userId, String query)
        {
            List<Message> result = new List<Message>();
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List(userId);
            request.Q = query;

            do
            {
                try
                {
                    ListMessagesResponse response = request.Execute();
                    result.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));

            foreach (Message message in result)
            {
                Console.WriteLine(message.Payload);
            }

            return result;
        }

        public static IList<Message> GetMessages(GmailService service, String userId, String query)
        {
            var emailListRequest = service.Users.Messages.List(userId);
            emailListRequest.LabelIds = "INBOX";
            emailListRequest.IncludeSpamTrash = false;
            emailListRequest.Q = query; 

            //get our emails
            var emailListResponse = emailListRequest.Execute();
            IList<Message> messages = emailListResponse.Messages;


            return messages;
        }

        public static void PrintMessage(Message email, GmailService service, String userId)
        {
            if (email != null)
            {
                var emailInfoRequest = service.Users.Messages.Get(userId, email.Id);
                var emailInfoResponse = emailInfoRequest.Execute();

                if (emailInfoResponse != null)
                {
                    String snippet = emailInfoResponse.Snippet;
                    Console.WriteLine();
                    Console.WriteLine("Message: " + snippet);

                    String from = "";
                    String date = "";
                    String subject = "";

                    //loop through the headers to get from,date,subject, body  
                    foreach (var mParts in emailInfoResponse.Payload.Headers)
                    {
                        if (mParts.Name == "Date")
                        {
                            date = mParts.Value;
                        }
                        else if (mParts.Name == "From")
                        {
                            from = mParts.Value;
                        }
                        else if (mParts.Name == "Subject")
                        {
                            subject = mParts.Value;
                        }

                        if (date != "" && from != "")
                        {
                            if (emailInfoResponse.Payload.Parts != null)
                            {
                                foreach (MessagePart p in emailInfoResponse.Payload.Parts)
                                {
                                    if (p.MimeType == "text/html")
                                    {
                                        byte[] data = FromBase64ForUrlString(p.Body.Data);
                                        string decodedString = Encoding.UTF8.GetString(data);
                                        Console.WriteLine(decodedString);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public static byte[] FromBase64ForUrlString(string base64ForUrlInput)
        {
            int padChars = (base64ForUrlInput.Length % 4) == 0 ? 0 : (4 - (base64ForUrlInput.Length % 4));
            StringBuilder result = new StringBuilder(base64ForUrlInput, base64ForUrlInput.Length + padChars);
            result.Append(String.Empty.PadRight(padChars, '='));
            result.Replace('-', '+');
            result.Replace('_', '/');
            return Convert.FromBase64String(result.ToString());
        }

        [Test]
        public void PrintFirstAndLastMessageFromInbox()
        {
          
            //print first and last message from first inbox tab
            GmailService service = GmailAuthorization.GetGmailService();
            IList<Message> messages = GetMessages(service, UserId, "");
            PrintMessage(messages[0], service, UserId);
            PrintMessage(messages[messages.Count - 1], service, UserId);

                        
        }

        [Test]
        public void PrintFirstMessageFromAuthor()
        {
            GmailService service = GmailAuthorization.GetGmailService();
            //print first message from certain author
            IList<Message> messageFromAuthor = GetMessages(service, UserId, Query);
            PrintMessage(messageFromAuthor[messageFromAuthor.Count-1], service, UserId);
        }
        
    }
}